<?php

error_reporting(0);
ini_set('display_errors', 0);

require_once('config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');


$errors = array();

if(isset($_REQUEST['logout'])) {
	$_SESSION['login_id'] = null;
	$_SESSION['login_name'] = null;
	$_SESSION['login_type'] = null;
	$_SESSION['login_offices'] = null;
	$_SESSION['basket'] = null;
	$_SESSION['level'] = null;

	$_SESSION = array();
	session_destroy();

	$logoutOk = true;
}

$email = '';

if (isset($_GET['error'])&&$_GET['error']==1)
	$errors[] = 'To arrange access to view the data room please <a href="mailto:info@haslams.co.uk">contact us</a> directly or <A HREF="javascript:javascript:history.go(-1)">click here</a> to go back to where you were';

if(isset($_REQUEST['operation_submit']) AND $_REQUEST['operation_submit'] == '1') {
	
	$_SESSION['login_id'] = 0;

	if(isset($_REQUEST['email']) AND strlen($_REQUEST['email']) < 4) $errors[] = 'E-mail empty/too short';
	if(isset($_REQUEST['password']) AND strlen($_REQUEST['password']) < 3) $errors[] = 'Password empty/too short';

	/*if (md5($_REQUEST['password']) == '0fad1e5a6dbd5dafb8567c0a538694ce' AND $_REQUEST['email']=='michael@tlgd.co.uk') 
	{
		
		if(isset($_REQUEST['ret_url']) AND strlen($_REQUEST['ret_url']) > 4)
				header('Location: '.urldecode($_REQUEST['ret_url']));
			else
				header('Location: /protected/index.php?file=index.html');
			exit;
		
	}*/
	if(count($errors) == 0) {
		$sql = 'SELECT * FROM users WHERE LCASE(username) = \''.mysql_real_escape_string(strtolower($_REQUEST['username'])).'\' AND BINARY(password) = BINARY(\''.mysql_real_escape_string($_REQUEST['password']).'\') AND meta_status = \'live\'';
		$db->query($sql);
		
		if($db->num_rows() != 0) {
			$db->next_record();

			$_SESSION['login_id'] = intval($db->f('meta_id'));
			$_SESSION['login_name'] = $db->f('forename').' '.$db->f('surname');
			$_SESSION['login_type'] = $db->f('meta_type') == 'admin' ? 'admin' : 'user';
			$_SESSION['level'] = $db->f('level');
			$usersOffice = $db->f('office');

			$sql = 'UPDATE users SET meta_lastlogin = NOW() WHERE meta_id = '.intval($_SESSION['login_id']);
			$db->query($sql);
			
			$sql = 'INSERT INTO log_login VALUES ('.$_SESSION['login_id'].', NOW());';
			//echo $sql;
			$db->query($sql);


			// get a list of the offices that the user can view properties for
			$_SESSION['login_offices'] = array(); // reset first
			if($usersOffice == '3')
				$_SESSION['login_offices'] = array(3); // Farnham
			else {
				// load in all other offices
				// this could join with a database table of user-specific offices, but for now it's hard-coded
				$sql = 'SELECT * FROM offices WHERE meta_status = \'published\' AND meta_id != 3';
				$db->query($sql);
				while($db->next_record()) $_SESSION['login_offices'][] = intval($db->f('meta_id'));
			}


			if(isset($_REQUEST['ret_url']) AND strlen($_REQUEST['ret_url']) > 4)
				header('Location: '.urldecode($_REQUEST['ret_url']));
			else
				header('Location: protected/dataroom.php');
			exit;
		}
		
		else
			$errors[] = 'Sorry, we were unable to log you in. Please verify your details (passwords are case-sensitive) and try again';
		
	}
	
}
elseif(isset($_REQUEST['submitpwd']) AND $_REQUEST['submitpwd']) {
	if(isset($_REQUEST['emailpwd']) AND strlen($_REQUEST['emailpwd']) < 4) $errors[] = 'E-mail address empty/too short';

	if(count($errors) == 0) {
		$credentialsInfo = array();
		$sql = 'SELECT * FROM users WHERE email = \''.mysql_real_escape_string($_REQUEST['emailpwd']).'\' AND meta_status = \'live\'';
		$db->query($sql);
		if($db->num_rows() != 0) {
			$db->next_record();
			$accountFound = true;
			$email = $db->f('email');
			$password = $db->f('password');
			$name = $db->f('forename').' '.$db->f('surname');

			$credentialsInfo[] = '';
			$credentialsInfo[] = '<strong>User Log In Details:</strong>';
			$credentialsInfo[] = 'Your e-mail address is: '.htmlspecialchars($email);
			$credentialsInfo[] = 'Your password is: '.htmlspecialchars($password).' (remember this is case-sensitive)';

			$mailMessage = array();
			$mailMessage[] = 'This e-mail contains your log in details.';
			$mailMessage[] = '';
			$mailMessage[] = 'You can log in here: <a href="http://'.$_SERVER['SERVER_NAME'].'/login/index.php?email='.htmlspecialchars($email).'">http://'.$_SERVER['SERVER_NAME'].'/login/index.php</a>';
			$mailMessage = array_merge($mailMessage, $credentialsInfo);

			$emailSubject = 'Lost Password Request';

			// HTML message body
			#$htmlMsg = file_get_contents('/domains/d/e/dev.deveregardens.com/public_html/page-components/email-template.html');
			#$htmlMsg = str_replace('<%%SUBJECT%%>', $emailSubject, $htmlMsg);
			#$htmlMsg = str_replace('<%%CONTENT%%>', implode('<br />', $mailMessage), $htmlMsg);
			#$htmlMsg = str_replace('<%%SERVER_NAME%%>', $_SERVER['SERVER_NAME'], $htmlMsg);

			$txtMsg = $emailSubject."\n\n".strip_tags(str_replace('<br />', "\n", implode("\n", $mailMessage)))."\n\n\nhttp://".$_SERVER['SERVER_NAME'].'/';

			$result = mail($email, $emailSubject, $txtMsg, 'From: '._WEBSITE_EMAIL_ADDRESS_);

			if($result)
				$passwordSentOk = 'Password sent successfully; please check your e-mail.<br /><br />The e-mail will have come from '.htmlspecialchars(_WEBSITE_EMAIL_ADDRESS_).' so if you don\'t receive it then please check your spam folder or add that address to your white-list and try again';
			else
				$errors[] = 'Unfortunately we were unable to send your password.';
		}
		else
			$errors[] = 'Unfortunately we were unable to find a matching account or maybe your account has been disabled.';
	}
}
elseif(isset($_REQUEST['email']) AND $_REQUEST['email'] != '')
	$email = $_REQUEST['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$shortTitle?> - Commercial Property Agency &amp; Consultancy Reading, Haslams</title>

	<meta name="keywords" content="<?= $title ?>">
	<meta name="description" content="<?= $title ?>">

	<link href="protected/assets/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="protected/css/custom.css" type="text/css" rel="stylesheet" />

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script type="text/javascript" src="//use.typekit.net/rww1sfn.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
	<div class="container" style="height: 100%;">
		<img src="protected/images/haslams-logo.png" alt="Haslams - Commercial Property Agency &amp; Consultancy Reading" class="hlogo"	/>
		<div class="login">
			
			<form method="post" action="" id="details">			
				
				<h1><span>Dataroom</span><?=$shortTitle?></h1>
				<input type="text" name="username" value="" placeholder="USERNAME"  />
				<input type="password" name="password" value="" placeholder="PASSWORD" />
				
				<div class="clearfix">
					<a class="pull-left requestpw" href="mailto:info@haslams.co.uk?subject=Haslams Data Room - <?=$shortTitle?> - Password Request" title="Haslams Commercial Property Agency - Password Request">REQUEST A PASSWORD</a>
					<input type="submit" name="submit" value="LOGIN" class="btn pull-right" id="subBut" />
				</div>
				<input type="hidden" name="ret_url" id="ret_url" value="<?php if(isset($_REQUEST['ret_url']) AND $_REQUEST['ret_url'] != '') echo htmlspecialchars($_REQUEST['ret_url']); ?>" />
				<input type="hidden" name="operation_submit" id="operation_submit" value="1" onClick="protected/index.html" />
				<?php if(count($errors) > 0) echo '<strong>'.showMessages($errors).'</strong>'; ?>
				<?php if(isset($logoutOk)) echo '<strong>'.showMessages(array('You have logged out successfully')).'</strong>'; ?>
				<?php if(isset($passwordSentOk)) echo '<strong>'.showMessages(array($passwordSentOk)).'</strong>'; ?>
			</form>
		</div>
		

  	</div>

	<!--JQUERY-->
	<script type="text/javascript" src="protected/js/jquery.js"></script>
	<script src="protected/js/placeholder.js"></script>

	<!--BOOTSTRAP		
	<script type="text/javascript" src="protected/assets/bootstrap/js/bootstrap.js"></script> --> 

	
</body>
</html>