// JavaScript Document

		
	$(window).load(bgInit);	
		
function bgInit()
{
	jQuery('#background img').each(function()
	{
		iw = jQuery(this).width();			// image width
		ih = jQuery(this).height();			// image height
		var pw = jQuery(window).width();	// page width
		var ph = jQuery(window).height();	// page height
		//alert(jQuery('#background img:first').width());
		resizeBg(iw, ih, pw, ph,jQuery(this));
	});
}
// resize on window resize
jQuery(window).resize(function() {
  bgInit();
});
//resize background image
function resizeBg(iw, ih, pw, ph,img) { //args: image width, image height, page width, page height */
  //alert(iw+' '+ih+' '+pw+' '+ph);
  if (ih > ph && iw < pw) {
    img.css('width', pw+'px');
    img.css('height', ((ih*pw)/iw)+'px');
  } else if (ih < ph && iw > pw) {
    img.css('height', ph+'px');
    img.css('width', ((ph*iw)/ih)+'px');
  } else if (ih > ph && iw > pw) {
    if (((ih*pw)/iw) >= ph) {
      img.css('width', pw+'px');
      img.css('height', ((ih*pw)/iw)+'px');
    } else {
      img.css('height', ph+'px');
      img.css('width', ((ph*iw)/ih)+'px');
    }
  } else if (ih < ph && iw < pw) {
    if (((ih*pw)/iw) >= ph) {
      img.css('width', pw+'px');
      img.css('height', ((ih*pw)/iw)+'px');
    } else {
      //alert((ph*iw));
	  img.css('height', ph+'px');
      img.css('width', ((ph*iw)/ih)+'px');
    }
  }
}