<?php

error_reporting(0);
ini_set('display_errors', 0);

 require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
    //ini_set('include_path',BASE_PATH.'/includes/');
    if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

// from http://uk.php.net/filesize
function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
}

function outputFiles($path)
{
    $output = '';
    foreach (glob($path.'/*.*') as $filename) {    	
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank">'.htmlspecialchars($outputFilename.' - '.format_bytes(filesize($filename))).'</a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

function outputImages($path)
{
    $output = '';
    foreach (glob($path.'/*.*') as $filename) {  
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/large/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank"><img src="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" alt="'.htmlspecialchars($outputFilename).'"></a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=$title;?> - Data room - Commercial Property Agency &amp; Consultancy Reading, Haslams</title>

	<meta name="keywords" content="Haslams Chartered Surveyors">
	<meta name="description" content="Data rooms for Commercial Property Agency &amp; Consultancy Reading, Haslams">


<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" type="text/css" rel="stylesheet" />


<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


	<script type="text/javascript" src="//use.typekit.net/rww1sfn.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body class="nobg">
    <nav>
   		<div class="container">
   			<div class="row">
				<div class="span7">
					<img src="images/haslams-logo-small.png" alt="Haslams - Commercial Property Agency &amp; Consultancy Reading"/>
					<h1><span>Dataroom:</span> <?=$shortTitle;?></h1>
				</div>
				<div class="span5">
					<ul>
						<li><a href="http://www.haslams.co.uk" target="_blank">Haslams Home</a></li>
						<li><a href="mailto:info@haslams.co.uk?subject=Haslams Chartered Surveyors  - <?=$shortTitle;?> - Enquiry" class="contact highlight" title="Haslams Chartered Surveyors - Contact us">Contact Us</a></li>
						<li><a href="/index.php?logout=1" class="contact" title="Haslams Chartered Surveyors - Log Out" >Log Out</a></li>

				</div>				
			</div>
		</div>
   </nav>
   

<div class="container">
	<div class="row">
		<div class="span3">

		    <ul class="nav nav-tabs nav-stacked" id="myTab">
			 	<li><h2>Dataroom</h2></li>
				<li class="active top"><a href="#t1" >1. Land Registry Freehold Title and Title Plan and Associated Documents</a></li>
				<li><a href="#t2">2. Existing Plans</a></li>
				<li><a href="#t3">3. Energy Performance Certificate</a></li>
                <li><a href="#t4">4. Asbestos Survey</a></li>
                <li><a href="#t5">5. LPA Pre-app Advice, Planning Application, Proposed Site Layout and Accommodation Schedule</a></li>
                <li><a href="#t6">6. Environmental Report</a></li>
                <li><a href="#t7">7. Historic Plans</a></li>
			</ul>  
		     <p class="added">You will need to have adobe acrobat installed to view some of these documents, <a href="http://www.adobe.com/products/acrobat.html" target="_blank">click here</a> if you do not have it installed</p>
		</div>
	
		<div class="span9">
			<div class="tab-content">

		    	<div class="tab-pane active" id="t1">
					<h2>1. Land Registry Freehold Title and Title Plan and Associated Documents</h2>
					<ul>
					    <?php outputFiles('download/1. Land Registry'); ?>
					</ul>
				</div>

				<div class="tab-pane" id="t2">
					<h2>2. Existing Plans</h2>
					<ul>
					    <?php outputFiles('download/2. Existing Plans'); ?>
					</ul>
                </div>
		    
				<div class="tab-pane" id="t3">
					<h2>3. Energy Performance Certificate</h2>
					<ul>
					    <?php outputFiles('download/3. Energy Performance Certificate'); ?>
					</ul>	
					
				</div>

                <div class="tab-pane" id="t4">
                    <h2>4. Asbestos Survey</h2>
                    <ul>
                        <?php outputFiles('download/4. Asbestos Survey'); ?>
                    </ul>

                </div>

                <div class="tab-pane" id="t5">
                    <h2>5. LPA Pre-app Advice, Planning Application, Proposed Site Layout and Accommodation Schedule</h2>
                    <ul>
                        <?php outputFiles('download/5. LPA'); ?>
                    </ul>

                </div>

                <div class="tab-pane" id="t6">
                    <h2>6. Environmental Report</h2>
                    <ul>
                        <?php outputFiles('download/6. Environmental Report'); ?>
                    </ul>

                </div>

                <div class="tab-pane" id="t7">
                    <h2>7. Historic Plans</h2>
                    <ul>
                        <?php outputFiles('download/7. Historic Plans'); ?>
                    </ul>

                </div>

            </div>
		</div>
	</div>
</div>
	<!--JQUERY-->
	<script type="text/javascript" src="js/jquery.js"></script> 
	<!--BOOTSTRAP--> 	
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script> 
	<script>
		$(function () {
			$('#myTab a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
			});	
		});
	</script>	
	<script>
		// Javascript to enable link to tab
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		} 
		
		// Change hash for page-reload
		$('.nav-tabs a').on('shown', function (e) {
			window.location.hash = e.target.hash;
		});
	</script>
</body>
</html>
