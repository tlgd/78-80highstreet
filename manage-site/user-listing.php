<?php
error_reporting(0);
ini_set('display_errors', 0);


require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
require_once(BASE_PATH.'/includes/class.dblister.php');


if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }
//print_r($_SESSION); exit();
if($_SESSION['level']<3) { header('Location: /index.php'); exit('Access Denied');  }

$itemType = 'user';

$orderByCol = $orderByDir = '';

$userLookup = new dbLister('users', 'user', $orderByCol != '' ? $orderByCol : 'meta_inserted', 6, 'desc', array('name' => 'surname', 'company' => 'company', 'inserted' => 'meta_inserted'));
//$propertyLookup = new dbLister('properties', 'property', $orderByCol != '' ? $orderByCol : 'meta_updated', 10, $orderByDir != '' ? $orderByDir : 'desc', array('name' => 'name', 'price' => 'lowest_area_price', 'town' => 'town','updated' => 'meta_updated', ));
$userLookup->addExtraColumn('meta_inserted_readable', 'DATE_FORMAT(meta_inserted, \'%e %M %Y\')');
//$userLookup->addExtraColumn('office_name', '(SELECT offices.title FROM offices WHERE offices.meta_id = office AND offices.meta_status != \'deleted\')');
$userLookup->whereCondition = 'meta_status != \'deleted\'';
$userLookup->setNumberOfResults();
$paging = $userLookup->generatePaging();
$userLookup->getMatches();

//echo $_REQUEST['submit'];

if (isset($_REQUEST['submit_confirm']) AND isset($_REQUEST['submit_email'])){


//************************************Emailing******/START/**************************
				$password = $_REQUEST['submit_password'];
				$to = $_REQUEST['submit_email']; //"michael@tlgd.co.uk, dainius.dilba@gmail.com"; //.$email; for client + cc michael
				$from = "info@quartermileinvestment.com";
				$subject = "Login details for Quartermile";

				//begin of HTML message
				$message = <<<EOF
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Quartermile - Welcome</title>
				</head>
				
				<body style="background-color:#242424;">
				<div style="font-family:Helvetica, Arial, sans-serif; font-size:14px;">
				<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#000000" style="border:2px solid #000;">
				  <tr>
				    <td><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/header.gif" alt="Quartermile" /></td>
				  </tr>
				  <tr>
				    <td><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgHead.jpg" alt="View of development from the meadows" /></td>
				  </tr>
				  <tr>
				    <td>
				    <table width="600" cellspacing="0" cellpadding="0">
				  <tr>
				    <td valign="top"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgLeft.jpg" /></td>
				    <td>
				    <table width="400" cellspacing="0" cellpadding="3" bgcolor="#d7d7d7">
				  <tr>
				    <td colspan="3" height="20px">&nbsp;</td>
				  </tr>
				  <tr>
				    <td rowspan="5" width="20px">&nbsp;</td>
				    <td style="font-size:24px; color:#555555; letter-spacing:-1px;font-family:Helvetica, Arial, sans-serif;"><strong>Welcome</strong></td>
				    <td rowspan="5" width="20px">&nbsp;</td>
				  </tr>
				  <tr>
				  
				    <td style="height:10px;"></td>
				   
				  </tr>
				  <tr>
				    
				    <td style="color:#555555; line-height:1.5;font-family:Helvetica, Arial, sans-serif;">You have been invited to view details of the prestigious <strong>Quartermile</strong> development opportunity. Please use the login details and link below to begin:</td>
				  
				  </tr>
				  <tr>
				  
					<td style="height:20px;"></td>
				  
				  </tr>
				  <tr>
				  
				    <td>
				    <table width="360" cellspacing="0" cellpadding="0">
				  <tr>
				    <td style="color:#555555;font-family:Helvetica, Arial, sans-serif;" width="100px">Email adress</td>
				    <td style="color:#000; font-size:16px; font-weight:bold;font-family:Helvetica, Arial, sans-serif;"><a href="#" style="color:#000; text-decoration:none;">$to</a></td>
				  </tr>
				  <tr>
				    <td height="10px" colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
					<td style="color:#555555;font-family:Helvetica, Arial, sans-serif;">Password</td>
				    <td style="color:#000; font-size:16px; font-weight:bold;font-family:Helvetica, Arial, sans-serif;">$password</td>
				  </tr>
				  <tr>
				       <td height="10px" colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
					<td style="color:#555555;font-family:Helvetica, Arial, sans-serif;">Link</td>
				    <td style="color:#000; font-size:16px;font-family:Helvetica, Arial, sans-serif;"><a href="http://www.quartermileinvestment.com" style="color:#000">www.quartermileinvestment.com</a></td>
				  </tr>
				</table>
				    </td>
				  
				  </tr>
				   <tr>
				    <td colspan="3" height="30px">&nbsp;</td>
				  </tr>
				</table>
				    </td>
				    <td valign="top"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgRight.jpg" /></td>
				  </tr>
				</table>
				    </td>
				  </tr>
				  <tr>
				    <td height="50px">&nbsp;</td>
				  </tr>
				  <tr>
				    <td height="2px" bgcolor="#a70920"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				  </tr>
				  <tr>
				    <td height="10px"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				  </tr>
				    <tr>
				    <td>
				    <table width="600" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="10px"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				    <td style="font-size:12px; color:#FFF;font-family:Helvetica, Arial, sans-serif;">Any questions please contact <a href="mailto:andrew.smith@cbre.com?=Quartermile Welcome Email" style="color:#FFF">andrew.smith@cbre.com</a> or <a href="mailto:adam.cradick@cbre.com?=Quartermile Welcome Email" style="color:#FFF">adam.cradick@cbre.com</a></td>
				  </tr>
				</table>
				    </td>
				  </tr>
				  <tr>
				  <td height="20px"></td>
				  </tr>
				</table>
				</div>
				</body>
				</html>				
EOF;
				
				//end of message
				$headers  = "From: $from\r\n";
				$headers .= "Content-type: text/html\r\n";
			
				//options to send to cc+bcc
				//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]";
				//$headers .= "Bcc: [email]email@maaking.cXom[/email]";
			    
				// now lets send the email.
				mail($to, $subject, $message, $headers);
				//echo 'mail('.$to.', '.$subject.', '.$message.', '.$headers.')';
				//echo "Message has been sent....!"
			  
				//************************************Emailing*****/END/***************************


}



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Agents Insight - Dataroom</title>


<link href="css/dataroom-edit.css" rel="stylesheet" type="text/css" />


</head>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script>
function changelevel(obj,fpid)
{
	var f_status='';
	//if($(obj).is(':checked'))
	f_status = $(obj).val();
	
	//console.log(f_status);
	$.post('changelevel.php',{ id:fpid,status:f_status},
	function(data)
	{
	});
}
</script>
<body id="addCompany">
<div id="header">
	<?php include('./header.php'); ?>
</div>
        
<div class="yellow"></div>

<div id="body_wrapper">			
            
	<h1><?php echo $sections[$itemType]['desc']; ?></h1>
            
            <ul id="breadcrumb">
		<li><a href="#">Dashboard</a></li>
                <li>/</li>
		<li class="active"><?php echo $sections[$itemType]['desc']; ?></li>	            
  </ul>
       <?php if(isset($_REQUEST['msg']) AND $_REQUEST['msg'] != '') echo showMessages(array($_REQUEST['msg'])); ?>
             
  <div class="yellowunBound1">
            <div class="floatRight">
		<a href="<?php echo $sections[$itemType]['edit-filename']; ?>?operation=add" class="active" title="Create a <?php echo $sections[$itemType]['item-name']; ?>">+ Add <?php echo $sections[$itemType]['item-name']; ?></a></li>
		<a href="#">Cancel</a>

	    
  </div>
</div>

<div id="formContainer">

<div id="panel_nav_container">

		<div id="navigation">
        	<ul class="yellowStrip">
        		<li><a href="user-listing.php">Users</a>
                    	<ul>
                    		<li><a href="user-listing.php" class="active">User List</a></li>
                  	</ul>
                    
                  	</li>
                  	<li><a href="preview-report1.php">Reports</a></li>
                </ul>
                </div>
</div>
 
   <div id="adminSideRight" class="marginb">        
   
   <div id="downarrow_grey_one"><img src="images/maindown_arrow_1.gif" width="25" height="10" /></div>
   
   	<h2>User List</h2>
    
    <p>This is a list of users who have access to <span class="yellowTxt">Dataroom</span></p>
          
   </div> 
   
<div id="sortbybar">
  <h3><!--<a href="#">Bulk Actions</a>--></h3>            
  <select name="sort_by" id="sort_by" onchange="JavaScript: if(this.selectedIndex != 0) window.location = this.options[this.selectedIndex].value" class="selectbox_1">
<?php
	$sortOptions = array('name' => 'surname', 'company' => 'company'); // the keys in this list have to be the same as the values in the 6th argument of "new dbLister()". // , 'size' => 'Size from'
	//if(isset($filters['location'])) $sortOptions['distance'] = 'Distance';

	echo '<option'.((!isset($_REQUEST['ordercol']) OR $_REQUEST['ordercol'] == '' OR !isset($_REQUEST['order']) OR $_REQUEST['order'] == '') ? ' selected="selected"' : '').'>Sort your properties</option>';
	foreach($sortOptions AS $urlArg => $desc) {
		echo '<option'.((isset($_REQUEST['ordercol']) AND $_REQUEST['ordercol'] == $urlArg AND isset($_REQUEST['order']) AND $_REQUEST['order'] == 'asc') ? ' selected="selected"' : '').' value="'.$userLookup->getSortLink($urlArg, 'asc').'&amp;displaytype='.urlencode($displayType).'">'.htmlspecialchars($desc).' (ascending)</option>';
		echo '<option'.((isset($_REQUEST['ordercol']) AND $_REQUEST['ordercol'] == $urlArg AND isset($_REQUEST['order']) AND $_REQUEST['order'] == 'desc') ? ' selected="selected"' : '').' value="'.$userLookup->getSortLink($urlArg, 'desc').'&amp;displaytype='.urlencode($displayType).'">'.htmlspecialchars($desc).' (descending)</option>';
	}
?>
	</select>
<div style="clear:both;"></div>
                
                </div>
  
 <div id="listcontainer">

<!--<ul>
	<div class="yellowunBound1">
   
   		<ul id="sort_list">
   		  <li>SORT BY:</li>
        		<li><a href="#"><?php echo ucwords($sections[$itemType]['item-name']); ?> Surname</a></li>
                <li><a href="<?php echo $userLookup->getSortLink('surname', 'asc'); ?>"><img src="images/up_arrow.gif" name="articleup" width="10" height="8" id="articleup" onmouseover="MM_swapImage('articleup','','images/up_arrow_ro.gif',1)" onmouseout="MM_swapImgRestore()" /></a></li>

                <li><a href="<?php echo $userLookup->getSortLink('surname', 'desc'); ?>"><img src="images/down_arrow.gif" name="articledown" width="10" height="8" id="articledown" onmouseover="MM_swapImage('articledown','','images/down_arrow_ro.gif',1)" onmouseout="MM_swapImgRestore()" /></a></li>
          <li><a href="#">Date Added</a></li>
                <li><a href="<?php echo $userLookup->getSortLink('inserted', 'asc'); ?>"><img src="images/up_arrow.gif" name="updatedup" width="10" height="8" id="updatedup" onmouseover="MM_swapImage('updatedup','','images/up_arrow_ro.gif',1)" onmouseout="MM_swapImgRestore()" /></a></li>
                <li><a href="<?php echo $userLookup->getSortLink('inserted', 'desc'); ?>"><img src="images/down_arrow.gif" name="updateddown" width="10" height="8" id="updateddown" onmouseover="MM_swapImage('updateddown','','images/down_arrow_ro.gif',1)" onmouseout="MM_swapImgRestore()" /></a></li>
	  </ul> 

  </div>
  <div class="clear"></div> -->

<?php 
if(is_array($userLookup->rowsToDisplay) AND count($userLookup->rowsToDisplay)) foreach($userLookup->rowsToDisplay AS $row) {
	$sql = 'SELECT count(*) as numbers from log_download WHERE user_id ='.$row['meta_id'];
	$db->query($sql);
	$db->next_record();
		$downloads = intval($db->f('numbers'));
		
	$sql = 'SELECT count(*) as numbers from log_login WHERE user_id ='.$row['meta_id'];
	$db->query($sql);
	$db->next_record();
		$logins = intval($db->f('numbers'));
	
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post" enctype="multipart/form-data">';
		echo '<input type="hidden" name="submit_confirm" id="submit_confirm" value="1" />';
		echo '<input type="hidden" name="submit_email" id="submit_email" value="'.$row['email'].'" />';
		echo '<input type="hidden" name="submit_password" id="submit_email" value="'.$row['password'].'" />';
		echo '<table class="datatable">';
		echo '<tr '.($row['meta_status'] == 'live' ? '' : 'class="live"').'>';
		echo '<td width="416">';
		echo '<p><h2><a class="whitelink_1" id="inline" href="'.$sections[$itemType]['edit-filename'].'?id='.intval($row['meta_id']).'&amp;operation=edit" class="editbut" onclick="$(this).fancybox().trigger(\'click\'); return false;">'.htmlspecialchars($row['forename'].' '.$row['surname']).', ';
		if (isset($row['company'])) echo $row['company'];
		else echo '';
		echo '</a></h2></p>';
		echo '			<p>No. of Downloads: <span class="whiteTxt">'.$downloads.'</span> No. of times accessed: <span class="whiteTxt">'.$logins.'</span></p>';
		echo '		</td>';
		echo '		<td width="177" align="right">';
		echo '		<select id="level" name="level" onchange="changelevel(this, '.intval($row['meta_id']).')">';
      		echo '		<option value="" >Select Level</option>';
      		echo '		<option value="1" ';
      		if (intval($row['level'])==1 ) echo 'selected="selected"';
      		echo ' >1</option>';
      		echo '		<option value="2" ';
      		if (intval($row['level'])==2 ) echo 'selected="selected"';
      		echo '>2</option>';
      		echo '		<option value="3" ';
      		if (intval($row['level'])==3 ) echo 'selected="selected"';
      		echo '>3</option>';
      		echo '		</select>';
      		echo '		</td>';
		echo '		<td width="177" align="right">';
		echo '			<select id="generic_dropdown" onchange="JavaScript: if(this.selectedIndex != 0) window.location = this.options[this.selectedIndex].value" title="Sort by">';
        	echo '		<option selected="selected">Discussed</option>';
        	echo '		<option value="#">Proposal Sent</option>';
        	echo '		<option value="#">Not sent yet</option>';
        	echo '	</select>';
		echo '		</td>';
		//echo '<td><input type="submit" value="Send Email"></td>';
		echo '		<td width="68"><a href="'.$sections[$itemType]['view-filename'].'?id='.intval($row['meta_id']).'"><img src="images/view_btn_full.png" alt="View" name="Image" width="60" height="25" id="Image" title="View" /></a></td>';
		echo '		<td width="39" class="last">';
		echo '			<a href="#" onClick="confirm(\'Are you sure you want to delete this user?\')"><img src="images/icons/bin_icon.png" width="18" height="22" alt="Delete" title="Delete" /></a>';
		echo '		</td>';
		echo '</tr>';
		echo '</table>';
		echo '</form>';
	}
?>

        
		<?php
			if(count($paging) > 0) {
				$pagingOutput = '<ul id="pages">'."\n";
				if(count($paging)) {
					$i = 0;
					$pageLinks = array();
					foreach($paging as $key => $urlDetails) {
						//if($i++ != 0) $pagingOutput .= '<li>,</li>'."\n";
						$display = key($urlDetails);
						$href = $urlDetails[$display];
						if($display == '...')
							$pageLinks[] = '<li>'.$display.'</li>'."\n";
						elseif($href != '')
							$pageLinks[] = '<li><a href="'.$href.'">'.$display.'</a></li>'."\n";
						else
							$pageLinks[] = '<li><span class="page-on">'.$display.'</span></li>'."\n";
					}
				}
				echo $pagingOutput .= implode("\n", $pageLinks).'</ul>';
			}
		?>
     </div>


<div class="clear"></div>
   </div>

           
       <div class="yellowunBound1">
            <div class="floatRight">
		<a href="<?php echo $sections[$itemType]['edit-filename']; ?>?operation=add" class="active" title="Create a <?php echo $sections[$itemType]['item-name']; ?>">+ Add <?php echo $sections[$itemType]['item-name']; ?></a></li>
       		<a href="#">Cancel</a>
       </div>
       </div>




</div>

<div id="footer">
<p><a href="#">Help ?</a></p>
</div>

</body>
</html>
