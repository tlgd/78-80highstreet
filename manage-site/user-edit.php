<?php

require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');

if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }
if($_SESSION['level']<3) { header('Location: /index.php'); exit('Access Denied');  }
$itemType = 'user';

$id = 0;
if(isset($_REQUEST['id']) AND intval($_REQUEST['id']) > 0) $id = intval($_REQUEST['id']);

$tableName = 'users';

$fileTypes = array(); // names of the file inputs

$errors = array();

// check that an operation has been passed
if(!isset($_REQUEST['operation'])) { header('Location: user-listing.php?msg='.urlencode('Invalid operation')); exit; }
switch(strtolower($_REQUEST['operation'])) {
	case 'edit': { $operation = 'edit'; break; }
	case 'add': { $operation = 'add'; break; }
	default: { die('invalid operation'); break; }
}

if(isset($_POST['submit_confirm']) AND $_POST['submit_confirm'] == '1') {
	$forename = $_POST['forename'];
	$surname = $_POST['surname'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	$email = $_POST['email'];
	$username = $_POST['username'];
	$office = 1;
	$usertype = 'user';
	$level = $_POST['level'];
	$company = $_POST['company'];;

	//if(!isValidEmail($email)) $errors[] = 'Please enter a valid email address';
	if(strlen($usertype) < 2) $errors[] = 'Please choose a user type';
	if(strlen($forename) < 2) $errors[] = 'Please choose a longer forename';
	if(strlen($surname) < 2) $errors[] = 'Please choose a longer surname';
	if(strlen($password) > 0 OR strlen($password2) > 0) {
		if(strlen($password) < 4) $errors['password'] = 'Password too short - minimum length is 8 characters';
		elseif(strlen(preg_replace('|[^0-9]|i', '', $password)) < 2) $errors['password'] = 'You must include at least 2 numbers in your password';
		elseif(strlen(preg_replace('|[^A-Z]|i', '', $password)) < 2) $errors['password'] = 'You must include at least 2 letters in your password';
		elseif($password != $password2) $errors['password'] = 'Passwords do not match';
	}
	elseif($id == 0)
		$errors['password'] = 'Password too short';
	if(intval($office) == 0) $errors[] = 'Please choose an office';

	if(count($errors) == 0) {
		$valuesSql  = 'forename = \''.mysql_real_escape_string($forename).'\', ';
		$valuesSql .= 'surname = \''.mysql_real_escape_string($surname).'\', ';
		if($password != '') $valuesSql .= 'password = \''.mysql_real_escape_string($password).'\', ';
		$valuesSql .= 'email = \''.mysql_real_escape_string($email).'\', ';
		$valuesSql .= 'office = '.intval($office).', ';
		$valuesSql .= 'meta_type = \''.mysql_real_escape_string($usertype).'\', ';
		$valuesSql .= 'level = '.intval($level).', ';
		$valuesSql .= 'company = \''.mysql_real_escape_string($company).'\', ';
		$valuesSql .= 'username = \''.mysql_real_escape_string($username).'\', ';
		$valuesSql .= 'meta_updated = NOW() ';

		switch($operation) {
			case 'add': {
				$sql  = 'INSERT INTO '.$tableName.' SET ';
				$sql .= $valuesSql.', ';
				$sql .= 'meta_status = \'live\', ';
				$sql .= 'meta_inserted = NOW()';
				$msg = ucwords($sections[$itemType]['item-name']).' added successfully';
				$db->query($sql);
				$id = $db->last_insert_id();
				
				
				//************************************Emailing******/START/**************************
	
				$to = $email; //"michael@tlgd.co.uk, dainius.dilba@gmail.com"; //.$email; for client + cc michael
				$from = "info@lsq-site.com";
				$subject = "Login details for Quartermile";

				//begin of HTML message
				$message = <<<EOF
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Quartermile - Welcome</title>
				</head>
				
				<body style="background-color:#242424;">
				<div style="font-family:Helvetica, Arial, sans-serif; font-size:14px;">
				<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#000000" style="border:2px solid #000;">
				  <tr>
				    <td><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/header.gif" alt="Quartermile" /></td>
				  </tr>
				  <tr>
				    <td><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgHead.jpg" alt="View of development from the meadows" /></td>
				  </tr>
				  <tr>
				    <td>
				    <table width="600" cellspacing="0" cellpadding="0">
				  <tr>
				    <td valign="top"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgLeft.jpg" /></td>
				    <td>
				    <table width="400" cellspacing="0" cellpadding="3" bgcolor="#d7d7d7">
				  <tr>
				    <td colspan="3" height="20px">&nbsp;</td>
				  </tr>
				  <tr>
				    <td rowspan="5" width="20px">&nbsp;</td>
				    <td style="font-size:24px; color:#555555; letter-spacing:-1px;font-family:Helvetica, Arial, sans-serif;"><strong>Welcome</strong></td>
				    <td rowspan="5" width="20px">&nbsp;</td>
				  </tr>
				  <tr>
				  
				    <td style="height:10px;"></td>
				   
				  </tr>
				  <tr>
				    
				    <td style="color:#555555; line-height:1.5;font-family:Helvetica, Arial, sans-serif;">You have been invited to view details of the prestigious <strong>Quartermile</strong> development opportunity. Please use the login details and link below to begin:</td>
				  
				  </tr>
				  <tr>
				  
					<td style="height:20px;"></td>
				  
				  </tr>
				  <tr>
				  
				    <td>
				    <table width="360" cellspacing="0" cellpadding="0">
				  <tr>
				    <td style="color:#555555;font-family:Helvetica, Arial, sans-serif;" width="100px">Email adress</td>
				    <td style="color:#000; font-size:16px; font-weight:bold;font-family:Helvetica, Arial, sans-serif;"><a href="#" style="color:#000; text-decoration:none;">$email</a></td>
				  </tr>
				  <tr>
				    <td height="10px" colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
					<td style="color:#555555;font-family:Helvetica, Arial, sans-serif;">Password</td>
				    <td style="color:#000; font-size:16px; font-weight:bold;font-family:Helvetica, Arial, sans-serif;">$password</td>
				  </tr>
				  <tr>
				       <td height="10px" colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
					<td style="color:#555555;font-family:Helvetica, Arial, sans-serif;">Link</td>
				    <td style="color:#000; font-size:16px;font-family:Helvetica, Arial, sans-serif;"><a href="http://www.quartermileinvestment.com" style="color:#000">www.quartermileinvestment.com</a></td>
				  </tr>
				</table>
				    </td>
				  
				  </tr>
				   <tr>
				    <td colspan="3" height="30px">&nbsp;</td>
				  </tr>
				</table>
				    </td>
				    <td valign="top"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/imgRight.jpg" /></td>
				  </tr>
				</table>
				    </td>
				  </tr>
				  <tr>
				    <td height="50px">&nbsp;</td>
				  </tr>
				  <tr>
				    <td height="2px" bgcolor="#a70920"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				  </tr>
				  <tr>
				    <td height="10px"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				  </tr>
				    <tr>
				    <td>
				    <table width="600" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="10px"><img src="http://quartermile.tlgd.co.uk/protected/mailers/confirmation/images/spacer.gif" /></td>
				    <td style="font-size:12px; color:#FFF;font-family:Helvetica, Arial, sans-serif;">Any questions please contact <a href="mailto:andrew.smith@cbre.com?=Quartermile Welcome Email" style="color:#FFF">andrew.smith@cbre.com</a> or <a href="mailto:adam.cradick@cbre.com?=Quartermile Welcome Email" style="color:#FFF">adam.cradick@cbre.com</a></td>
				  </tr>
				</table>
				    </td>
				  </tr>
				  <tr>
				  <td height="20px"></td>
				  </tr>
				</table>
				</div>
				</body>
				</html>				
EOF;
				
				//end of message
				$headers  = "From: $from\r\n";
				$headers .= "Content-type: text/html\r\n";
			
				//options to send to cc+bcc
				//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]";
				//$headers .= "Bcc: [email]email@maaking.cXom[/email]";
			    
				// now lets send the email.
			//Commented by client request!!!//------!!!mail($to, $subject, $message, $headers);       
				//echo 'mail('.$to.', '.$subject.', '.$message.', '.$headers.')';
				//echo "Message has been sent....!"
			  
				//************************************Emailing*****/END/***************************
				
				
				break;
			}
			case 'edit': {
				if(count($errors) == 0) {
					$sql  = 'UPDATE '.$tableName.' SET ';
					$sql .= $valuesSql.' ';
					$sql .= 'WHERE meta_id = '.intval($id);
					$msg = ucwords($sections[$itemType]['item-name']).' edited successfully';
					$db->query($sql);
				}
				break;
			}
		}

		if(count($errors) == 0) {
			if(count($errors) == 0) {
				header('Location: user-view.php'.'?id='.$id.'&msg='.urlencode($msg));
				exit;
			}
		}
	}
}
elseif(intval($id) > 0) {
	$sql = 'SELECT * FROM '.$tableName.' WHERE meta_id = '.intval($id).' AND meta_status != \'deleted\'';
	$db->query($sql);
	if($db->num_rows() != 0) {
		$db->next_record();
		$forename = $db->f('forename');
		$surname = $db->f('surname');
		$email = $db->f('email');
		$office = $db->f('office');
		$usertype = $db->f('meta_type');
		$level = $db->f('level');
		$company = $db->f('company');
		$username = $db->f('username');
		
	}
	else
		die('No matching item found');
}
else {
	$operation = 'add';

	$forename = '';
	$surname = '';
	$email = '';
	$office = 1;
	$usertype = 'user';
	$level = 1;
	$company = '';
	$username = '';
	
	$password = "";
	$possible = "bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
	$possible2 = "1234567890";
	$maxlength = strlen($possible);
	$maxlength2 = strlen($possible2);
	$i = 0;
	while ($i <= 2){
		$char = substr($possible, mt_rand(0, $maxlength-1), 1);
		$char2 = substr($possible2, mt_rand(0, $maxlength2-1), 1);
		if (!strstr($password, $char)) { 
			$password .= $char;
			// ... and increase the counter by one
			$i++;
		}
		if (!strstr($password, $char2)) { 
			$password .= $char2;
			// ... and increase the counter by one
		}
	}


}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>CMS - <?php echo $sections[$itemType]['desc'].' - '.htmlspecialchars($forename.' '.$surname); ?></title>

<link href="css/cms.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="header">
	<?php include('./header.php'); ?>
</div>
        
<div class="yellow"></div>

<div id="body_wrapper">			
            
	<h1><?php echo $sections[$itemType]['desc']; ?></h1>
            
            <ul id="breadcrumb">
		<li><a href="index.php">Dashboard</a></li>
                <li>/</li>
		<li><a href="<?php echo $sections[$itemType]['index-filename']; ?>"><?php echo $sections[$itemType]['desc']; ?></a></li>
                <li>/</li>
		<li><?php echo htmlspecialchars($forename != '' ? $forename.' '.$surname : 'Add new '.$sections[$itemType]['item-name']); ?></li>
  </ul>
            
  <div class="clear"></div>
  
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">

	<?php if(count($errors) > 0) echo showMessages($errors); ?>

  <div id="button_wrapper">
	  <ul id="buttons">
		  <li class="form_button"><label for="button"></label><input name="button" type="submit" class="form_button" id="button" value="SAVE" />
		  </li>
      </ul>
        
    <ul id="back_button">
			<li><a href="<?php echo $sections[$itemType]['index-filename']; ?>">Back</a></li>
  		</ul>

  </div>
  
  <div class="clear2"></div>
  
<div id="intro">
  		<p>Editing your profile details could not be easier. Once you have altered the details that you wish to change, simply click <strong>SAVE</strong> above and your details will be saved.</p>
  </div>
  
  <div class="clear2"></div>
  
  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Email</h5>

      <input name="email" type="text" class="box" id="email" size="50" value="<?php echo htmlspecialchars($email); ?>">
      
    <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Login</h5>

      <input name="username" type="text" class="box" id="username" size="40" value="<?php echo htmlspecialchars($username); ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  

  <!--<div class="profilebox_alt">
  
	  <h5>User Type</h5>

		<?php //echo outputSelect('usertype', $userTypes, $usertype); 
		?>
      
      <div id="info2"><img src="images/icons/info.png" id='user_type' width="19" height="19" /></div>
  
  </div> -->
  
  <input type="hidden" name="usertype" value="user">

  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Forename</h5>

      <input name="forename" type="text" class="box" id="forename" size="40" value="<?php echo htmlspecialchars($forename); ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Surname</h5>

      <input name="surname" type="text" class="box" id="surname" size="40" value="<?php echo htmlspecialchars($surname); ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Password<?php if($id != 0) echo ' (leave blank to leave unchanged)'; ?></h5>

      <input name="password" type="password" class="box" id="password" size="40" value="<?php if (isset($password)) echo $password; ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Re-enter Password</h5>

      <input name="password2" type="password" class="box" id="password2" size="40" value="<?php if (isset($password)) echo $password; ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>

  <!--
   <div class="profilebox">
  
	  <h5>Office</h5>

      <?php //echo outputSelect('office', loadOffices(), $office); 
      ?>
      
    <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
-->

<div class="profilebox">
  
	  <h5>User Level</h5>

	  <select id="level" name="level">
      		<option value="" >Please select</option>
      		<option value="1" <?php if ($level =='1') echo 'selected="selected"'; ?> >1</option>
      		<option value="2" <?php if ($level =='2') echo 'selected="selected"'; ?> >2</option> 
      		<option value="3" <?php if ($level =='3') echo 'selected="selected"'; ?> >3</option>
      	</select>
      
    <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
</div>

<div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Company</h5>

      <input name="company" type="text" class="box" id="company" size="40" value="<?php echo htmlspecialchars($company); ?>">
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>


<input type="hidden" name="office" value="1">

 <div id="button_wrapper">
	  <ul id="buttons">
		  <li class="form_button"><label for="button"></label><input name="button" type="submit" class="form_button" id="button" value="Save" />
		  </li>
  		</ul>

        
    <ul id="back_button">
			<li><a href="<?php echo $sections[$itemType]['index-filename']; ?>">Back</a></li>
  		</ul>
  </div>
  
	<input type="hidden" name="operation" id="operation" value="<?php echo htmlspecialchars($operation); ?>" />
	<input type="hidden" name="submit_confirm" id="submit_confirm" value="1" />
	<input type="hidden" name="id" id="id" value="<?php echo intval($id); ?>" />
	<input type="hidden" name="ret_url" id="ret_url" value="<?php if(isset($_REQUEST['ret_url'])) echo htmlspecialchars($_REQUEST['ret_url']); ?>" />
  </form>


  </div>
<div class="clear"></div>

<div class="yellow2"></div>
<div id="footer">
	
</div>

</body>
</html>
