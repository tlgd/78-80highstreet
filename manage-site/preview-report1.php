<?php
ob_start();
require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
require_once(BASE_PATH.'/includes/class.dblister.php');
require_once(BASE_PATH.'/manage-site/pdf/dompdf_config.inc.php');

if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

if($_SESSION['level']<3) { header('Location: /index.php'); exit('Access Denied');  }

if (isset($_POST['from']) && $_POST['from']<>"") $from = $_POST['from'];
	else $from = 1;
	
if (isset($_POST['to'])) $to = $_POST['to'];
	else $to = $from+9;

$i=0;
$end = 1;	
$total = 0;	
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style>

body {
  color:#000;
  font-family:Arial, Helvetica, sans-serif;
  font-size:12px;
  padding:6px;
  margin:0;
}

.circle li { 
list-style-type: circle; 
padding: 0;

}

table.info {
	border-width: 1px;
	border-color: #000;
	border-style: solid;
	background-color:#fff;
	border-collapse:collapse;
}
table.info td{
	border-width: 1px;
	border-color: #000;
	border-style: solid;
	height: 15px;
	border-collapse:collapse;
	padding-top: 20px;
}


</style>

</head>

<body>

<div style="width:578px; height:829px; margin-bottom:6px; background-color:#000;">
 <table width="578" border="0" cellspacing="0" cellpadding="0" style="padding:0; margin:0; background-color:#000; width:578px; ">
  <tr>
    <td style="width:300px; height:72px;"><img src="'.'/home/t/l/tlgmarco/public_html'.'/manage-site/pdf/www/images/wandilogo.jpg" width="171px" height="72px" style="padding:0; margin:0; width:171px; height:72px;" /></td>
  </tr>
  <tr>
  	<td style="width:300px; height:72px;">
  		<p style="font-family:sans-serif; color:#999; font-size:15px; padding:40px 0 0 15px; margin:0; text-align:left;"><strong>Website User &amp; Download Report</strong><br />DATE RANGE: FROM - TO</p>    
  	</td>
  </tr>
</table>
</div>

	<div style="width:578px;">

		<div style="width:578px; background-color:#000; padding-top:10px; text-align:center; font-size:15px; font-family:sans-serif; color:#fff;" >
			<p style="border-top:10px solid #000; border-left:10px solid #000;">There have been _ active users on the website to date</p>
		</div>
		
		
		<div style="width:578px; padding-top:5px; text-align:center; color:#fff; background-color:#000; font-size:10px; font-family:sans-serif;" >
			<p style="border-top:5px solid #000; border-left:5px solid #000;">Visitor Overview</p>
		</div>
		
		
			<table width="578" class="info" cellspacing="0"">
				<tr>
					<td width="225px" >Company</td>
					<td width="141px">Person</td>
					<td width="141px">Email</td>
					<td width="65px">Login Count</td>
					<td width="65px">Page Views</td>
					<td width="65px">Downloads</td>
				</tr>
';

if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }
 
	if(isset($_REQUEST['submit_confirm'])) {
		$displaydateTimestamp1 = ukDateToTimestamp($_REQUEST['datefrom']);
		$displaydateTimestamp2 = ukDateToTimestamp($_REQUEST['dateto'].' 23:59:59');
		//echo $displaydateTimestamp1.' end : '.$displaydateTimestamp2; exit();
		  if (isset($_REQUEST['users']) AND $displaydateTimestamp1 > '' AND $displaydateTimestamp2 > ''){
		  	  $fp = fopen(BASE_PATH."/manage-site/report1.csv", "w");
		  	  $list = array ('Company', 'Person','Email', 'Login Count', 'Page Views', 'Downloads');
			  fputcsv($fp, $list);
			  foreach ($_REQUEST['users'] as $key => $value){
				$sql = 'select
							users.forename, 
							users.surname,
							users.email,
							users.company,
							users.bidderlist,
							(select count(*) as logins from log_login where log_login.user_id = users.meta_id AND log_login.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_login.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as logins,
							(select count(*) as pages from log_pages where log_pages.user_id = users.meta_id AND log_pages.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_pages.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as pages,
							(select count(*) as downloads from log_download where log_download.user_id = users.meta_id AND log_download.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_download.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as downloads
							from users
							WHERE meta_id = '.$value;
					$db->query($sql);
					$i=0;
					while ($db->next_record()){
						$i++;
						//$list = array ($db->f('company'), $db->f('forename').' '.$db->f('surname'), $db->f('logins'), $db->f('pages'),  $db->f('downloads'));
						//fputcsv($fp, $list);
						$html .= '<tr>';
						$html .= '<td width="225px">'.$db->f('company').'</td>';
						$html .= '<td width="141px">'.$db->f('forename').' '.$db->f('surname').'</td>';
						$html .= '<td width="65px" >&nbsp;'.$db->f('email').'</td>';
						$html .= '<td width="65px" >&nbsp;'.$db->f('logins').'</td>';
						$html .= '<td width="65px">&nbsp;'.$db->f('pages').'</td>';
						$html .= '<td width="65px">&nbsp;'.$db->f('downloads').'</td>';
						$html .= '</tr>';
						
					}
			  }
			  //fclose($fp);
			  $html .= '
			  </table>
		
		
	
	</div>

</body>

</html>
				
				';
		  }
		  if($_REQUEST['submit_next'] == 'Download Report')
			{
				//echo $html;
				$dompdf = new DOMPDF();
				$dompdf->load_html($html);
				$dompdf->render();
				$dompdf->stream("test.pdf");
			}
  }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Agents Insight - Dataroom</title>


<link href="css/dataroom-edit.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <!-- add for multiselect -->
        <script type="text/javascript" src="js/jquery.multiselect.js"></script>

<script type="text/javascript">
$(function() {
		
	// Datepicker
				$('.dates').datepicker({
					showOn: "button",
			buttonImage: "images/icons/calendar_icon.png",
			buttonImageOnly: true
				});
		
	});
</script>

<script type="text/javascript">

$(function(){

	$("select.multi").multiselect();

});

</script>
<script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  document.forms['specform'].elements['from'].value = 1;
  document.forms['specform'].submit();
}
function MM_jumpMenu2(targ,selObj,restore){ //v3.0
	document.forms['specform'].elements['from'].value = <?php echo (intval($from-$to)<=0 ? '1' : intval($from-$to)) ?>;
	document.forms['specform'].submit();
}
function MM_jumpMenu3(targ,selObj,restore){ //v3.0
	document.forms['specform'].elements['from'].value = <?php echo $from+$to ?>;
	document.forms['specform'].submit();
}
</script>
</head>

<body id="addCompany">
<div id="header">
<div id="header_wrapper">

                <div id="logo"><img src="images/logo.png"></div>

                <ul id="name">

					
                    <li class="white" style="width:80px;">Michael Avery</li>

                 
                                <li class="downarrow_1">Admin</li>
                                
                               <li class="white">|</li>
					   <li class="settings_1">Settings</li>
                               

    </ul>

				 <ul class="the_menu_1 the_menu">
                                    <span class="white1"><strong>Settings</strong></span>
                                    <li><a href="specification-listing.php">Edit Specifications</a></li>
                                    

    </ul>
<ul class="the_menu_2 the_menu">
                                    <span class="white1"><strong>Admin Links</strong></span>
                                    <li><a href="http://tlgd.zendesk.com" target="_blank">Support Website</a></li>
                                    <li><a href="mailto:support@tlgd.zendesk.com">Contact Support</a></li>
                                    <li><a href="/index.php?logout=1">Log out</a></li>

    </ul>




	


                
  </div>
</div>

<div id="body_wrapper">

<h1>Dataroom</h1>

 <ul id="breadcrumb">
    <li><a href="#">Dataroom /</a></li>
     <li><a href="#">Reports /</a></li>
     <li><a href="#" class="active">User Overview</a></li>	            
    </ul>
    
       <div class="yellowunBound1">
            <div class="floatRight">
            <a href="download.php?file=report1.csv" class="active">Download</a>
       		<a href="user-listing.php">Cancel</a>
       </div>
           </div>
          

<div id="formContainer">

<div id="panel_nav_container">

		<div id="navigation">
        		<ul class="yellowStrip">
                    <li><a href="user-listing.php">Users</a></li>
                    <li><a href="preview-report1.php">Reports</a></li>
                    <ul>
                    	  <li><a href="preview-report1.php" class="active">User Overview</a></li>
                    	  <li><a href="preview-report2.php">User Detail</a></li>
                    	
                    	  <li><a href="preview-report4.php">Downloads Overview</a></li>
                  	  	</ul>
                </ul>
        </div>
</div>
 
   <div id="adminSideRight" class="marginb">        
   
   <div id="downarrow_grey_one"><img src="images/maindown_arrow_1.gif" width="25" height="10" /></div>
   <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="specform" name="specform"> 
   	<h2>Your Report</h2>
    
    <dl class="addDetails">
      <div class="clear"></div>
    
    <table width="350" border="0" cellspacing="0" cellpadding="0" class="reportsummary">
  <tr>
    <th scope="row" width="80px">User(s):</th>
    	<td>
    	<?php
    	$assignedusers = array();
    	if (isset($_REQUEST['users']))
    		$assignedusers = $_REQUEST['users'];
    	echo outputSelect('users', getUsers(true), $assignedusers, false, false, 'multi', false, null, false, true); 
    	?>
    	</td>
  </tr>
  <tr>
    <th scope="row" width="80px">Date from:</th>
    <td><input type="text" name="datefrom" class="dates" value="<?php if (isset($_REQUEST['datefrom'])) echo $_REQUEST['datefrom']; ?>"></td>
  </tr>
  <tr>
    <th scope="row" width="80px">Date to:</th>
    <td><input type="text" name="dateto" class="dates" value="<?php if (isset($_REQUEST['dateto'])) echo $_REQUEST['dateto']; ?>"></td>
  </tr>

</table>
    <input type="hidden" name="submit_confirm" id="submit_confirm" value="1" />
    <input class="whiteButton margTop" type="submit" name="submit_next" id="submit_next" value="Update Report" />
    <!-- <input class="whiteButton margTop" type="submit" name="submit_next" id="submit_next" value="Download Report" /> -->
  	
    
    </dl>
    
   </div>
   
   <div id="listcontainer">
   
   <table width="720" border="0" cellspacing="0" cellpadding="0" id="reportstable">
  <tr>
    <th colspan="6" scope="col">Report Preview</th>
    </tr>
  <tr>
    <th scope="col" class="main" width="180px">Company</th>
    <th scope="col" class="main" width="180px">Person</th>
    <th scope="col" class="main" width="180px">Email</th>
    <th scope="col" class="main" width="160px">Login count</th>
    <th scope="col" class="main" width="100px">Page Views</th>
    <th scope="col" class="main">Downloads</th>
  </tr>
  <?php 
	if(isset($_REQUEST['submit_confirm'])) {
		$displaydateTimestamp1 = ukDateToTimestamp($_REQUEST['datefrom']);
		$displaydateTimestamp2 = ukDateToTimestamp($_REQUEST['dateto'].' 23:59:59');
		  if (isset($_REQUEST['users'])){
		  	  $total=0;
			  foreach ($_REQUEST['users'] as $key => $value){
				$sql = 'select
						users.forename, 
						users.surname,
						users.email,
						users.company,
						users.bidderlist,
						(select count(*) as logins from log_login where log_login.user_id = users.meta_id AND log_login.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_login.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as logins,
						(select count(*) as pages from log_pages where log_pages.user_id = users.meta_id AND log_pages.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_pages.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as pages,
						(select count(*) as downloads from log_download where log_download.user_id = users.meta_id AND log_download.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_download.date < FROM_UNIXTIME('.$displaydateTimestamp2.')) as downloads
						from users
						WHERE meta_id = '.$value;
				$db->query($sql);
				while ($db->next_record()){
						$result_set[$total]['company'] = $db->f('company');
						$result_set[$total]['forename'] = $db->f('forename');
						$result_set[$total]['surname'] = $db->f('surname');
						$result_set[$total]['email'] = $db->f('email');
						$result_set[$total]['logins'] = $db->f('logins');
						$result_set[$total]['pages'] = $db->f('pages');
						$result_set[$total]['downloads'] = $db->f('downloads');					
						$total++;
					}
				/*
				$i=0;
				while ($db->next_record()){
					$i++;
					$list = array ($db->f('company'), $db->f('forename'), $db->f('surname'), $db->f('logins').' '.$db->f('pages'),  $db->f('downloads'));
					if ($ip >= $from AND $ip <$from+$to) {
					?>
					<tr <?php if ($i==2) {echo 'class="greyone"'; $i=0; } ?> >
					    <td><?php echo $db->f('company');?></td>
					    <td><?php echo $db->f('forename').' '.$db->f('surname');?></td>
					    <td><?php echo $db->f('logins');?></td>
					    <td><?php echo $db->f('pages');?></td>
					    <td><?php echo $db->f('downloads');?></td>
					  </tr>
				<?php
					}
					$ip++;
				}
				*/
			  }
			  @sort($result_set);
			  $i=0;
			  $end = 1;
			  $ip = 1;
			  foreach ($result_set as $key => $value){
				$i++;
				$list = array ($value['company'], $value['forename'].' '.$value['surname'], $value['email'], $value['logins'], $value['pages'],  $value['downloads']);
				@fputcsv($fp, $list);
				//var_dump($ip >= $from AND $ip < $from+$to); 
				if ($ip >= $from AND $ip < $from+$to) {
				?>
					<tr <?php if ($i==2) {echo 'class="greyone"'; $i=0; } ?> >
					    <td><?php echo $value['company'];?></td>
					    <td><?php echo $value['forename'].' '.$value['surname'];?></td>
					    <td><?php echo $value['email'];?></td>
					    <td><?php echo $value['logins'];?></td>
					    <td><?php echo $value['pages'];?></td>
					    <td><?php echo $value['downloads'];?></td>
					  </tr>
				  <?php
				  }
				  $ip++;
			  }
			  @fclose($fp);
		  }
  }
?>
  
</table>

<div class="clear"></div>


<div id="bottom_sort">
        
        <p>Show rows:</p> 
        <!--<form name="form" id="sort_rows" action="" method="get">-->
        <select name="to" id="to" onchange="MM_jumpMenu('parent',this,0)" style="margin: 10px">
            <option value="10" <?php if ($to == 10) echo "selected" ?>>10</option>
            <option value="20" <?php if ($to == 20) echo "selected" ?>>20</option>
            <option value="30" <?php if ($to == 30) echo "selected" ?>>30</option>
          </select>
            
        <div id="page_sort">
        
        <?php if (isset($i) &&($i > 0)) 
        {
        	echo "<p>Showing ";
        
        	$end = ($ip>=$to) ? $from+$to-1 : $from+$ip-2; echo $from.' - '.$end;
        	
        	echo " of ".$total." results</p>";
        }
        else
        	echo "<p>No results</p>";
	if($end==1) $end = 10;
	//echo $total.' end : '.$end;
        ?>
 
        <input type="hidden" name="from" id="from" value="<?=intval($from)?>">
        
        <input type="button" id="next" title="Next" onclick="MM_jumpMenu3('parent',this,0)" <?php if($total<=$end) echo 'disabled="disabled"'; ?> >
        <input type="button" id="prev" title="Previous" onclick="MM_jumpMenu2('parent',this,0)">
        
        <!--<a id="prev" href="<? echo $_SERVER['PHP_SELF'].'?to='.intval($to).'&from='.(intval($from-$to)<=0 ? '1' : intval($from-$to)); ?>" title="Previous" onclick="MM_jumpMenu2('parent',this,0)"><span>Previous</span></a>-->         

        </div>
        
      </div>

   </div>

   <div class="clear"></div>
</div>
</form>
           
       <div class="yellowunBound1">
            <div class="floatRight">
            <a href="report1.csv" class="active" target="_blank">Download</a>
       		<a href="user-listing.php">Cancel</a>
       </div>
       </div>




</div>

<div id="footer">
<p><a href="#">Help ?</a></p>
</div>

</body>
</html>
